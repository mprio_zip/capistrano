Source: capistrano
Section: admin
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Samuel Henrique <samueloph@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               rake,
               ruby-airbrussh,
               ruby-i18n,
               ruby-mocha,
               ruby-rspec,
               ruby-sshkit
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/ruby-team/capistrano.git
Vcs-Browser: https://salsa.debian.org/ruby-team/capistrano
Homepage: https://capistranorb.com/
Testsuite: autopkgtest-pkg-ruby
Rules-Requires-Root: no

Package: capistrano
Architecture: all
Depends: ${ruby:Depends},
         ${misc:Depends},
         ${shlibs:Depends}
Description: tool to execute commands in parallel on multiple servers
 Capistrano is great for automating tasks via SSH on remote servers, like
 software installation, application deployment, configuration management,
 ad hoc server monitoring, and more. Ideal for system administrators, whether
 professional or incidental. Easy to customize. Its configuration files use
 the Ruby programming language syntax, but you don't need to know Ruby to do
 most things with Capistrano.
 .
 Capistrano is easy to extend. It's written in the Ruby programming language,
 and may be extended easily by writing additional Ruby modules.
