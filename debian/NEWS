capistrano (3.12.1-1) unstable; urgency=medium

  Upstream reverted the change to the variable git_wait_interval introduced in
  3.12.0, the default is now back to 0, so the only variable that needs to be
  changed to keep the old behavior overall is git_max_concurrent_connections,
  that can be accomplished by changing it to a number high enough (higher than
  the amount of servers to be deployed).

  The default values for the variables are:

  :git_max_concurrent_connections
    default: 10
    Number of concurrent connections to Git repository - useful when your Git
    server limits the number of simultaneous connections while using SSH (like
    Gitlab CE).

  :git_wait_interval
    default: 0
    Number of seconds to wait after you reach the limit of concurrent
    connections to Git repository server and disconnect afterwards to initialize
    new connections. This prevents from being cut out of SSH server when you use
    fail2ban or similar software for limiting connections to server.

 -- Samuel Henrique <samueloph@debian.org>  Sat, 14 Mar 2020 23:01:47 +0000

capistrano (3.12.0-1) unstable; urgency=medium

  Capistrano now defaults to limiting the number of concurrent git connections
  to 10 by default, this change has been made to make capistrano compatible
  with self-hosted Gitlab or similar services that defaults to 10 concurrent
  connections.

  You can control this behavior with the following variables:

  :git_max_concurrent_connections
    default: 10
    Number of concurrent connections to Git repository - useful when your Git
    server limits the number of simultaneous connections while using SSH (like
    Gitlab CE).

  :git_wait_interval
    default: 3
    Number of seconds to wait after you reach the limit of concurrent
    connections to Git repository server and disconnect afterwards to initialize
    new connections. This prevents from being cut out of SSH server when you use
    fail2ban or similar software for limiting connections to server.

 -- Samuel Henrique <samueloph@debian.org>  Wed, 12 Feb 2020 19:31:53 +0000

capistrano (3.2.1-1) unstable; urgency=low

  Capistrano 3.x introduces incompatible changes with previous 2.x
  configurations. For more information about upgrading, please see
  the official documentation http://capistranorb.com/documentation/upgrading/

 -- Sebastien Badia <seb@sebian.fr>  Sun, 01 Jun 2014 18:37:19 +0200
